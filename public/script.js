function is_valid_youtube_video_id(input_string) {
    if ( input_string.length != 11 ) {
        console.debug("[Video anonymizer] Video ID is not 11 characters long")
        return(false)
    }

    // YouTube video IDs contain only the following characters:
    //   Letters (upper- and lowercase)
    //   Numbers
    //   Underscore
    //   Hyphen
    if ( video_id.match(/[^0-9A-Za-z_-]/) ) {
        console.debug("[Video anonymizer] Video ID contains invalid character(s)")
        return(false)
    }

    return(true)
}

function get_iframe_dimensions(aspect_x, aspect_y) {
    if ( aspect_x > aspect_y ) {
        console.debug("[Video anonymizer] Landscape video aspect:", aspect_x, "x", aspect_y)
        iframe_width = window.innerWidth * 0.99
        iframe_height = iframe_width * ( aspect_y / aspect_x )
    } else {
        console.debug("[Video anonymizer] Portrait video aspect:", aspect_x, "x", aspect_y)
        iframe_height = window.innerHeight * 0.99
        iframe_width = iframe_height * ( aspect_x / aspect_y )
    }

    return([iframe_width, iframe_height])
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// Parse query string
var query_string_parameters = new URLSearchParams(window.location.search)

// Assign query string values to variables
var video_id        = query_string_parameters.get("v")
var aspect_x        = parseInt(query_string_parameters.get("aspect_x"))
var aspect_y        = parseInt(query_string_parameters.get("aspect_y"))
var want_controls   = parseInt(query_string_parameters.get("controls"))
var allowfullscreen = parseInt(query_string_parameters.get("allowfullscreen"))
var cover_title     = parseFloat(query_string_parameters.get("cover_title"))

// Make sure we've got what looks like a valid YouTube video ID
if ( ! is_valid_youtube_video_id(video_id) ) {
    alert("Invalid video ID")
    throw new Error("[Video anonymizer] Invalid video ID: " + video_id)
}

//
// Defaults if not specified in the query string
//

// Some example video IDs for defaults
//   LN3R1S_RYJU - Doom Eternal launch trailer
//   ygcAceTvoTk - Animal Crossing: New Horizons launch trailer
//   yhBImCpswSE - Portrait-orientation video for testing
if ( video_id        === null ) { video_id        = "dQw4w9WgXcQ" } // The original Rickroll (video not available)
if ( aspect_x        === null ) { aspect_x        = 16            } // Default aspect ratio of 16:9
if ( aspect_y        === null ) { aspect_y        = 9             } // Default aspect ratio of 16:9
if ( want_controls   === null ) { want_controls   = 1             } // Show controls by default
if ( allowfullscreen === null ) { allowfullscreen = 0             } // Don't allow fullscreen by default
if ( cover_title     === null ) { cover_title     = 1.0           } // Hide the video title with a bar by default

// Use the aspect ratio to calculate the largest possible IFRAME size
var [iframe_width, iframe_height] = get_iframe_dimensions(aspect_x, aspect_y)

// For troubleshooting
console.debug("[Video anonymizer] Video ID:", video_id)
console.debug("[Video anonymizer] Viewport dimensions:", window.innerWidth, "x", window.innerHeight)
console.debug("[Video anonymizer] IFRAME dimensions:", iframe_width, "x", iframe_height)

// Get the container div and the old IFRAME
var video_container = document.getElementById("video-container")
var old_iframe = video_container.lastElementChild

// Build the new IFRAME element
var new_iframe = document.createElement("iframe")
new_iframe.setAttribute("src",    "https://www.youtube.com/embed/" + video_id + "?controls=" + want_controls)
new_iframe.setAttribute("width",  iframe_width)
new_iframe.setAttribute("height", iframe_height)
if ( allowfullscreen == 1 ) {
    new_iframe.setAttribute("allowfullscreen", "allowfullscreen")
}

// Swap out the old IFRAME with the new one
video_container.replaceChild(new_iframe, old_iframe)

// Make the bar transparent if we're not using it, otherwise full opacity
// This is a clever misdirection; I make "cover_title" look like a Boolean
// value but in fact I send it straight to the opacity CSS, so you can use
// floating point values if you like.  Not sure why you'd _want_ to, but it's
// extra functionality for free, so why not?  Knowledge of this undocumented
// feature is your reward for reading the code!
var title_bar = document.getElementById("title-blocker").style.opacity = cover_title

// EOF
////////
