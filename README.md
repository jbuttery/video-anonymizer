Video anonymizer
================

This is a very simple, very hacky way to show someone a YouTube video without
showing them the title (for spoiler protection, etc.).  It's more cumbersome
than it needs to be, because YouTube is using IFRAMEs for its embedding now,
which means you can't use CSS to hide the title.  Thanks, Google!

Usage
-----

The basic idea is that you build a link to this tool, using the YouTube video
ID, and some extra options.  This process is mostly manual because YouTube
doesn't have an API you can access without signing up, and I can't be bothered
to spend the time scraping their Web site.

Anyway, getting on with it, first you start with the base URI for this tool and
add a query string with the parameters, like so:

`https://jbuttery.gitlab.io/video-anonymizer/?[...]`

Query string parameters
-----------------------

Required parameters:

Parameter         | Explanation
----------------- | -----------------------------------------------------------
`v`               | YouTube video ID
`aspect_x`        | Horizontal part of the aspect ratio
`aspect_y`        | Vertical part of the aspect ratio

Optional parameters:

Parameter         | Explanation
----------------- | -----------------------------------------------------------
`controls`        | Show controls (play/pause, progress bar, fullscreen, etc.)
`allowfullscreen` | Allow switching to fullscreen mode
`cover_title`     | Draw a black bar across the top to cover the title

You have to enter the aspect ratio manually because this tool has no way of
determining what the correct aspect ratio for the video is.  For a 16:9 aspect
ratio, you'd add `aspect_x=16&aspect_y=9` (see below for more examples).  You
can also just enter the dimensions of the video, if you know them (they'll
divide out to the same thing), for example: `aspect_x=1920&aspect_y=1080`.

You can turn the controls and ability to fullscreen on with `controls=1` and
`allowfullscreen=1`, respectively.  These are both off by default for
spoiler-protection reasons:

- Seeing the playback controls will tell you how long the video is and how much
  time is left.  This can be a spoiler, for example if you're watching a best
  of three sports match and you can tell the video isn't long enough for a
  third game.
- Going into fullscreen mode will put the video on top of the black bar that
  I've positioned over the top of the video.  This is the only way I could hide
  the video title, since YouTube removed the `showinfo` parameter.  Yes, this
  means the top 58 pixels of any video you show with this tool will be cut off.
  You can thank Google for removing the ability to hide the title in the player
  itself, and then forcing the use of IFRAME embedding to keep you from using
  your own CSS to style the player.

However, if you only need to hide the playback controls (you're probably just
hiding the progress bar) and the title isn't sensitive, you can disable the
black bar (and thus not lose the top 58 pixels of the video) with
`cover_title=0`.

Examples
--------

I expect you to have a basic understanding of how HTTP URIs work, so don't open
an issue in the tracker asking what `?` means or where to use `&`; I'll just
close them.  That said, no documentation is complete without examples.

First off, the initial use case: sending a video to a friend with maximum
spoiler protection.  These three examples all do exactly the same thing:

```
https://jbuttery.gitlab.io/video-anonymizer/?v=dQw4w9WgXcQ
https://jbuttery.gitlab.io/video-anonymizer/?v=dQw4w9WgXcQ&aspect_x=16&aspect_y=9
https://jbuttery.gitlab.io/video-anonymizer/?v=dQw4w9WgXcQ&aspect_x=1920&aspect_y=1080
```

The video in question here has an aspect ratio of 16x9 (the aspect ratio of
720p, 1080p, 2160p/4K, etc.).  I know I said earlier that the aspect parameters
were required; that was a terrible, terrible lie.  The default is set up for
16x9 because that's what the majority of YouTube videos are, but I didn't want
to assume that and have people wonder why some videos get rendered strangely.

The reason `&aspect_x=16&aspect_y=9` and `&aspect_x=1920&aspect_y=1080` both do
the same thing is that the only thing that matters is the _ratio_.  So, feel
free to use the ratio numbers or the actual video dimensions interchangeably
(the expressions `16/9` and `1920/1080` both produce `1.777`).

This also does exactly the same thing, because all the optional parameters are
specified with their default values:

`https://jbuttery.gitlab.io/video-anonymizer/?v=dQw4w9WgXcQ&aspect_x=16&aspect_y=9&controls=0&allowfullscreen=0&cover_title=1`

On the other hand, if you want to show the controls, allow fullscreen, and not
cover the title:

`https://jbuttery.gitlab.io/video-anonymizer/?v=dQw4w9WgXcQ&aspect_x=16&aspect_y=9&controls=1&allowfullscreen=1&cover_title=0`
